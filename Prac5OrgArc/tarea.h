/*
 *  * estructura de datos y funciones propias de la tarea
 */

/*
 * File:   tarea.h
 * Author: svera
 *
 * Created on 8 de septiembre de 2016, 11:33 AM
 */

#ifndef TAREA_H
#define TAREA_H
#define LMAX 150
#include <string.h>
#include "leeCadena.h"

//tipos de datos

typedef struct fecha {
    int dia;
    int mes;
    int anyo;
} fecha;

typedef struct tarea {
    int clave; // clave de la tarea
    fecha fi; // fecha de inicio, en la que la dejaron
    char mat[LMAX];
    fecha ff; // fecha final, la de entrega
    char titulo[LMAX];
} tarea;


/**
 * compara dos fechas, retorna un 0 si son iguales, comparaTarea
 * un valor negativo si f1 es menor que f2
 * un valor positivo si f1 es mayor que f2
 * @param f1
 * @param f2
 * @return
 */
int fchcmp(fecha f1, fecha f2) {
    int r = f1.anyo - f2.anyo;
    if (r != 0) {
        return r;
    }
    r = f1.mes - f2.mes;
    if (r != 0) {
        return r;
    }
    r = f1.dia - f2.dia;
    return r;
}

int menu(){
    int op;
    system("cls");
    printf("               .....::::REGISTRO DE TAREAS::::.....\n\n");
    printf("\t                     Menu:\n\n      1.Agregar tarea\n      2.Consultar tareas\n      3.Modificar tarea\n      4.Eliminar tarea\n      5.Salir\n");
    scanf("%d",&op);
    system("cls");
    return op;

}

tarea getTareaVacia(){
    tarea taux;
    taux.clave=-1;
    taux.ff.anyo=0;
    taux.ff.mes=0;
    taux.ff.dia=0;
    strcpy(taux.mat,"empty");
    strcpy(taux.titulo,"empty");
    taux.fi.anyo=0;
    taux.fi.mes=0;
    taux.fi.dia=0;
    return taux;
}

tarea pideTarea(void) {
    tarea taux;
    int cm, ct;
    printf("\n          ...Clave de la materia (ultimos dos digitos): ");
    scanf("%d", &cm);
    printf("\n          ...Numero de tarea (consecutivo por materia): ");
    scanf("%d", &ct);
    taux.clave= cm*10+ct;
    printf("\n             Fecha en la que dejaron la tarea: \n\n          ...Dia: ");
    scanf("%d", &taux.fi.dia);
    printf("\n          ...Mes: ");
    scanf("%d", &taux.fi.mes);
    printf("\n          ...Anio: ");
    scanf("%d", &taux.fi.anyo);
    printf("\n          ...Materia: ");
    leeCadena(taux.mat);
    printf("\n          ...Titulo de la tarea: ");
    leeCadena(taux.titulo);
    printf("\n             Fecha en la que se debe entregar la tarea: \n\n          ...Dia: ");
    scanf("%d", &taux.ff.dia);
    printf("\n          ...Mes: ");
    scanf("%d", &taux.ff.mes);
    printf("\n          ...Anio: ");
    scanf("%d", &taux.ff.anyo);
    return taux;
}



void dsplTarea(tarea t) {
    printf(" %d     %d/%d/%d          %s %s %d/%d/%d",t.clave, t.fi.dia, t.fi.mes, t.fi.anyo,t.mat, t.titulo,t.ff.dia, t.ff.mes, t.ff.anyo);
}

/**
 * compara dos tareas por fechas
 * @param l
 * @param t
 * @return
 */
int trcmpPorFecha(tarea t1, tarea t2) {
    int r = strcmp(t1.mat, t2.mat);
    if (r != 0) {
        return r;
    }
    r= fchcmp(t1.fi, t2.fi);
    if (r != 0) {
        return r;
    }
    r= fchcmp(t1.ff, t2.ff);
    if (r != 0) {
        return r;
    }
    r =strcmp(t1.titulo, t2.titulo);
    return r;
}


/**
 * compara dos tareas por clave
 * @param t1 primera tarea a comparar
 * @param t2 segunda tarea a comparar
 * @return
 */
int trcmp(tarea t1, tarea t2) {
    printf("cmpt:%d..",t1.clave-t2.clave);
    return (t1.clave-t2.clave);
}

#endif /* TAREA_H */

