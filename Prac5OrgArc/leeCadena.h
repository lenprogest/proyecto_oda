/*
 * funcion para la lectura de cadenas con espacios
 */

/*
 * File:   leeCad.h
 * Author: svera
 *
 * Created on 24 de agosto de 2016, 10:08 AM
 */

#ifndef LEECAD_H
#define LEECAD_H

/** función para leer una cadena con getchar*/
int leeCadena(char *cl) {
    int i = 0;
    char car;
    do{
        car = getchar();
    }while ((car == '\n')||(car == '\t')||(car == ' '));//quita blancos

    if (car != '\n') {
        cl[i] = car;
        i++;
    }

    while ((car = getchar()) != '\n') {
        cl[i] = car;
        i++;
    }
    //cl[i] = '\n';
    //i++;
    cl[i] = '\0';
    return i;
}
#endif /* LEECAD_H */

