#ifndef CONFIG_H
#define CONFIG_H
/**
 *	Header config.h que contiene definiciones de la longitud de campos utilizados en todo el programa,
 * 	tambien incluye definiciones de los nombres de los archivos que utiliza el programa.
 *
 **/

/**
 * 	Definicion de longitudes maximas.
 **/
#define L_MAX 50
#define MAX_PALABRA 20
#define MAX_DESCRIPCION 100
#define MAX_FUENTE 50

/**
 * 	Definicion de utilidades.
 **/
#define ERROR_FILE  -99
#define EMPTY 		-1
#define INGLES		0
#define ESPANIOL	1
#define ASSERT      1
#define FAIL        0
#define ESP_A_ING   3
#define ING_A_ESP   5

/**
 *	Definicion de los nombres de los archivos
 **/
#define nombre_archivo_dats "Dic.bin"
#define nombre_archivo_index "idx.bin"
#define nombre_archivo_trn "datTrn.bin"
#define nombre_archivo_inis "iniciosList.bin"

#endif
