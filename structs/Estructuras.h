#ifndef ESTRUCTURAS_H
#define ESTRUCTURAS_H
#define L_LANG 10
#include "config.h"
#include <string.h>

/**
 *	estructuras.h contiene las estructuras que son necesarias para el diccionario asi como
 * 	sus respectivas funciones para crear estructuras vacias.
 *
 **/



typedef struct diccionario {
		char idioma [L_LANG];
		char archivoTdatos [20];
		char archivoTindice [20];
		int indc_ini;
}diccionario;

typedef struct registro{
		char  palabra [MAX_PALABRA];
		char  descripcion [MAX_DESCRIPCION];
		char  fuente [MAX_FUENTE];
	}registro;

typedef struct traduccion {
		//char palabraOri[MAX_PALABRA];
		int posRegIng;
		int posRegEsp;
		int sigIng;
		int sigEsp;
}traduccion;

typedef	struct inicio{
	int inicioIndexEsp;
	int inicioIndexIngles;
}inicio;

typedef struct regindices{
		char Palabra[MAX_PALABRA];
		int sig;
        //# registro en tabla de datos
        int numReg_datos;
        //# registro en tabla de traducciones
        int numReg_trad;

}regindices;
/**
typedef struct regEsp{
		char  palabraEsp [MAX_PALABRA];
		char  descripciónEsp [MAX_DESCRIPCION];
		char fuenteEsp [MAX_FUENTE];
}regEsp;

typedef struct regIng{
		char  palabraIng [MAX_PALABRA];
		char  descripciónIng [MAX_DESCRIPCION];
		char fuenteIng [MAX_FUENTE];
}regIng;
*/

diccionario getDiccionarioVacio(){
    diccionario d;
    d.indc_ini = -1;


    strcpy(d.idioma,"empty");

    strcpy(d.archivoTindice,"empty");

    strcpy(d.archivoTdatos,"empty");

	return d;
}


registro getRegistroVacio(){
    registro r;

    strcpy(r.palabra,"empty");
    strcpy(r.descripcion,"empty");
    strcpy(r.fuente,"empty");

	return r;
}
/**
regEsp getRegEspañolVacio(){
    registro re;

    strcpy(re.palabra,"empty");
    strcpy(re.descripcion,"empty");
    strcpy(re.fuente,"empty");

	return re;
}

regIng getRegInglesVacio(){
    registro ri;

    strcpy(ri.palabra,"empty");
    strcpy(ri.descripcion,"empty");
    strcpy(ri.fuente,"empty");

	return ri;
}

*/

traduccion getTraduccionVacio(){
    traduccion t;
    //t.palabraOri[MAX_PALABRA] = {0};
    t.posRegEsp = -1;
    t.posRegIng = -1;
    t.sigEsp  = -1;;
    t.sigIng = -1;
	return t;
}

inicio getInicioVacio(){
    inicio i;
    i.inicioIndexEsp =-1;
    i.inicioIndexIngles = -1;
    return i;
}

regindices getIndicesVacio(){
    regindices rind;
    strcpy(rind.Palabra,"----");
    rind.sig = -1;
    rind.numReg_datos = -1;
    rind.numReg_trad = -1;
    return rind;
}



#endif // ESTRUCTURAS_H
