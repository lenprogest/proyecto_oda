###  README ###

## Proyecto Diccionario Español - Ingles ##

Se requiere un programa que simule diccionario ingles-español personal en el que este mismo pueda registrar una palabra ya sea en español o ingles junto con su definicion, donde tambien podra agregar la fuente de donde investigo la definicion.

El usuario podra registrar tambien palabras en ingles al igual que el registro en español, estos registros pueden tener una vinculacion entre palabras entre palabras en español con su respectiva traduccion a ingles de la palabra y definicion, al igual para las palabras en ingles tambien se podra consultar su traduccion al español de la palabra y definicion.