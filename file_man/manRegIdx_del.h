#ifndef MANREGIDX_DEL_H_INCLUDED
#define MANREGIDX_DEL_H_INCLUDED
#include <string.h>
#include "../structs/Estructuras.h"
#include "../structs/config.h"
#include "../file_man/modelo.h"



int buscaElim(char * palabra, int idioma ){
    if(strcmp(palabra,"----")!=0){
        return ASSERT;
    }else{
        return FAIL;
    }
}

int delUnico(regindices *regInex, int pos_list, int idioma){
    registro dummy_wrd = getRegistroVacio();
    regindices dummy_ri = getIndicesVacio();
    inicio * aux_i;
    inicio reg;
    aux_i = (inicio *)getRegistro(sizeof(inicio),pos_list,nombre_archivo_inis);

    if(idioma == ESPANIOL){
        (*aux_i).inicioIndexEsp = -1;
    }else{
        (*aux_i).inicioIndexIngles = -1;
    }
    reg = *aux_i;
    setRegistro(aux_i,(*regInex).Palabra[0]-97,nombre_archivo_inis);
    setRegistro(&dummy_ri,regInex->numReg_datos,nombre_archivo_index);
    setRegistro(&dummy_wrd,regInex->numReg_datos,nombre_archivo_dats);

    return ASSERT;
}

int delFrstElem(regindices * old_reg, int pos_list_ini, int idioma){
    registro dummy_wrd = getRegistroVacio();
    regindices aux_ridx = getIndicesVacio();
    inicio * aux_rstart = (inicio *)getRegistro(sizeof(inicio),pos_list_ini,nombre_archivo_inis);
    inicio fllw;
    if(idioma == ESPANIOL){
        (*aux_rstart).inicioIndexEsp = (*old_reg).sig;
    }else{
        (*aux_rstart).inicioIndexIngles = (*old_reg).sig;
    }
    fllw = *aux_rstart;
    setRegistro(aux_rstart,(*old_reg).Palabra[0]-97,nombre_archivo_inis);
    setRegistro(&aux_ridx,(*old_reg).numReg_datos,nombre_archivo_index);
    setRegistro(&dummy_wrd,(*old_reg).numReg_datos,nombre_archivo_dats);
    return ASSERT;
}

regindices AnteriorDel(char*palabra,char c,char * nom_Archivo, int idioma){
    regindices * raux;
    regindices * raux2;
    regindices x1,x2;

    raux2 = raux = (regindices *) getRegistro(sizeof(regindices),getInicio(c,idioma),nom_Archivo);
    while(strcmp(palabra,(*raux).Palabra)>0 && (*raux).sig != -1){
        raux2 = raux;
        raux = (regindices *) getRegistro(sizeof(regindices),(*raux).sig,nom_Archivo);
    }
    x1 = *raux;
    x2 = *raux2;
    return (*raux2);
}

int delBtwNodos(regindices * old_reg, int idioma){
    regindices ant = AnteriorDel((*old_reg).Palabra,(*old_reg).Palabra[0],nombre_archivo_index,idioma);
    regindices dummy = getIndicesVacio();
    registro dummy_wrd = getRegistroVacio();

    ant.sig = (*old_reg).sig;
    setRegistro(&ant,ant.numReg_datos,nombre_archivo_index);
    setRegistro(&dummy,(*old_reg).numReg_datos,nombre_archivo_index);
    setRegistro(&dummy_wrd,(*old_reg).numReg_datos,nombre_archivo_dats);
    return ASSERT;
}


int delFinal(regindices r, int idioma){
    regindices aux, dummy = getIndicesVacio();
    registro dummy_wrd = getRegistroVacio();

    aux = AnteriorDel(r.Palabra,r.Palabra[0],nombre_archivo_index,idioma);
    aux.sig = -1;
    setRegistro(&aux,aux.numReg_datos,nombre_archivo_index);
    setRegistro(&dummy,r.numReg_datos,nombre_archivo_index);
    setRegistro(&dummy_wrd,r.numReg_datos,nombre_archivo_dats);

        return ASSERT;
}



int eliminarReg(int lang, char * Palabra){
    regindices aux = buscaPalabra(Palabra,lang);
    regindices r;
    int getinicio=getInicio(Palabra[0],lang);
    if(getinicio!=-1){
        if(buscaElim(aux.Palabra,lang)==FAIL){
            printf("\tRegistro no encontrado!");
        }else{
            if(getinicio == aux.numReg_datos){
                if(aux.sig==-1){
                    ///TODO BORRAR UNICO
                    delUnico(&aux,getinicio,lang);
                    printf("\tBORRO UNICO!");

                }else{
                    delFrstElem(&aux,getinicio,lang);
                    printf("\tBORRO PRIM ELEM!");
                }
                return ASSERT;
            }
            if(aux.sig == -1){
                delFinal(aux,lang);
                printf("\tBORRO FINAL!");
                return ASSERT;
            }
            delBtwNodos(&aux,lang);
            return ASSERT;
        }
    }else{
        printf("\tRegistro no encontrado!");
        return FAIL;
    }

}

#endif // MANREGIDX_DEL_H_INCLUDED
