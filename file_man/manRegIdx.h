#ifndef MANREGIDX_H_INCLUDED
#define MANREGIDX_H_INCLUDED
#include <string.h>
#include "../structs/Estructuras.h"
#include "../structs/config.h"
#include "../file_man/modelo.h"





/**
 *	Funcion que actualiza el indicador de un inicio de alguna letra.
 *	parametros:
 *	int idioma ->diccionario correspondiente
    int pos -> posicion de registro del inicio de la letra correspondiente
    int inilista -> el inicio de la lista antes de que se modifique
    int new_inicio -> el nuevo valor del inicio de la lista
    char* nom_Archivo -> el nombre del archivo que contiene los indices
 *
    return ASSERT
**/

int setInicio(int idioma,int pos, int ini_lista,int new_Inicio,char *nom_archivo){
   //FILE * arch = fopen(nom_archivo,"rb+");
   inicio * rI;
   inicio rint;
   rI = (inicio*)malloc(sizeof(inicio));
   *rI=getInicioVacio();

   rI=(inicio*)getRegistro(sizeof(inicio),pos,nom_archivo);
   rint = *rI;
   if(idioma==ESPANIOL){
        rI->inicioIndexEsp=new_Inicio;
   }else{
    rI->inicioIndexIngles=new_Inicio;
   }
   rint = *rI;

   //if(arch!=NULL){
       // FILE * arch = fopen(nom_archivo,"rb+");

        //fseek(arch,0, SEEK_SET);
		//fread(&rI, sizeof(regIndex),1, arch);
    //despliegaArchivo();

    if(ini_lista==-1){
        setRegistro(rI,pos,nom_archivo);
        //despliegaArchivo();
    }else{
		rI = (inicio *) getRegistro(sizeof(inicio),pos,nombre_archivo_inis);
		if (idioma == ESPANIOL){
            (*rI).inicioIndexEsp = new_Inicio;
		}else{
            (*rI).inicioIndexIngles= new_Inicio;
        }
		rint = *rI;

		//fwrite(&rI,sizeof(regIndex),1,arch);//
		setRegistro(rI,pos,nombre_archivo_inis);
		//printf("%d",taux.cve)
		//despliegaArchivo();
		return ASSERT;
    }
}

/**
 *	Funcion que busca el registro anterior en una lista al registro dado
 *	parametros:
 *	regindices reg-> registro del que se desea buscar su anterior
    char*nom_Archivo-> nombre del archivo que va a utilñizar
    int idioma-> distingue entre diccionarios
 *
 * 	return *raux2
**/

regindices Anterior(regindices reg,char * nom_Archivo, int idioma){
    regindices * raux;
    regindices * raux2;
    regindices x1,x2;

    raux2 = raux = (regindices *) getRegistro(sizeof(regindices),getInicio(reg.Palabra[0],idioma),nom_Archivo);
    while(strcmp(reg.Palabra,(*raux).Palabra)>0 && (*raux).sig != -1){
        raux2 = raux;
        raux = (regindices *) getRegistro(sizeof(regindices),(*raux).sig,nom_Archivo);
    }
    x1 = *raux;
    x2 = *raux2;
    return (*raux2);
}

/**
 *	Funcion que contiene la logica de insercion de un elemento en una lista estando entre dos elementos de la lista
 *	parametros:
 *	regindices regNuevo-> registro nuevo a insertar
    char* nom_Archivo-> nombre del archivo a utilizar
    int idioma-> distingue entre diccionarios
 *
 * 	return regNuevo
**/

regindices Ins_EntreNodos(regindices regNuevo,char * nom_Archivo, int idioma){
    regindices regAnt = Anterior(regNuevo,nom_Archivo, idioma );
    regNuevo.sig = regAnt.sig;


    regNuevo.numReg_datos = numRegistrosExistentes(sizeof(regindices),nombre_archivo_index);

    //setInicio(idioma,regNuevo.Palabra[0]-97,getInicio(regNuevo.Palabra[0],idioma),regNuevo.numReg_datos,nom_Archivo);

    regAnt.sig = regNuevo.numReg_datos;

    setRegistro(&regAnt,regAnt.numReg_datos,nom_Archivo);
    return regNuevo;
}

/**
 *	Funcion que busca el ultimo registro de una lista
 *	char* palabra-> para buscar el inicio de la lista
    int idioma-> para distinguir entre diccionarios
 *
 * 	return reg
**/

regindices Ultimo(char *palabra, int idioma){
    int inicioLista;
    inicio * in;
    int nReg;
    regindices reg;
    //char = palabra [0]:
    nReg =  palabra[0] - 97; //(ASCII de A )
    in = (inicio*) getRegistro (sizeof (inicio), nReg, nombre_archivo_inis);
    if(idioma==ESPANIOL){
        inicioLista = in-> inicioIndexEsp;
    }
    else {
        inicioLista = in->inicioIndexIngles;
    }

    FILE * arch = fopen(nombre_archivo_index,"rb");
    fseek (arch,(sizeof(regindices))* inicioLista,SEEK_SET);
    fread (&reg, sizeof(regindices), 1, arch);
    while(reg.sig != -1){

        fseek(arch,sizeof(reg)*reg.sig,SEEK_SET);
        fread(&reg, sizeof(regindices), 1, arch);
    }
    return reg;
}

/**
 *	Funcion que contiene la logica de insercion de un elemento en al final de la lista
 *	parametros:
 *	regindices regN-> registro
    char* nom_Archivo-> nombre de archivo a utilizar
    int idioma-> distingue entre diccionarios
 *
 * 	return reg
**/

regindices Ins_Final(regindices reg,char * nom_Archivo, int idioma){
    regindices ultimoReg = Ultimo(reg.Palabra, idioma);
    reg.sig = -1;
    //reg.
    reg.numReg_datos = numRegistrosExistentes(sizeof(regindices),nom_Archivo);

    ultimoReg.sig = reg.numReg_datos;

    setRegistro(&ultimoReg,ultimoReg.numReg_datos,nom_Archivo);
    return reg;

}

/**
 *	Funcion que contiene la logica de insercion de un elemento en al inicio de la lista
 *	parametros:
 *	regindices regN-> registro
    char* nom_Archivo-> nombre de archivo a utilizar
    int idioma-> distingue entre diccionarios
 *
 * 	return reg
**/

regindices Ins_Inicio(int idioma,regindices regNuevo,char * nom_Archivo){

    regNuevo.sig = getInicio(regNuevo.Palabra[0],idioma);
    //reg.sig = getRegistro(0,nom_Archivo).inicio;

    regNuevo.numReg_datos = numRegistrosExistentes(sizeof(regindices),nom_Archivo);
    //getInicio()
    setInicio( idioma,regNuevo.Palabra[0]-97, regNuevo.sig,numRegistrosExistentes(sizeof(regindices),nombre_archivo_index),nom_Archivo);

    return regNuevo;
}

/**
 *	Funcion que evalua el resultado de una busqueda de una palbra
 *	parametros:
 *	char* palabra-> palabra que se mando a buscar
    int idioma-> distingue entre diccionarios
 *
 * 	return reg
**/

int buscaInsercion(char * palabra, int idioma ){
    //regindices * r = getRegistro(r.numReg_datos*sizeof(regindices),nom_arch);
    if(strcmp(palabra,"----")!=0){
        return ASSERT;
    }else{
        return FAIL;
    }
}

/**
 *	Funcion que distingue entre los cuatro casoso de insercion:
    primer elemento,insercion al final,al inicio, entre elementos
    Parametros:
    regindices r-> registro a insertar
    int lang-> idioma del diccionario
 *
 * 	return reg
**/

int buscaTipoIns(regindices r,int lang){

    int ini;
    regindices aux = buscaPalabra(r.Palabra,lang);
    regindices * r_aux;
    int getinicio=getInicio(r.Palabra[0],lang);

    //if(numRegistrosExistentes(sizeof(regindices),nombre_archivo_index)!=0){
    if(getinicio!=-1){
        if (buscaInsercion(aux.Palabra,lang) == FAIL){
                ///TODO Registro Encontrado
                printf("\tRegistro encontrado!\n");
        }else{

            aux = Ultimo(r.Palabra,lang);
            if(strcmp(r.Palabra,aux.Palabra)>0){
                ///_Ins final
                r = Ins_Final(r,nombre_archivo_index,lang);
                addRegistro(&r,sizeof(regindices),nombre_archivo_index);
                //printf("\tins final!\n");
                return ASSERT;
            }
            ini = getInicio(r.Palabra[0],lang);
            r_aux = (regindices *) getRegistro(sizeof(regindices),ini,nombre_archivo_index);

            if(strcmp(r.Palabra,(*r_aux).Palabra)<0){
            ///__ins inicio
                r = Ins_Inicio(lang,r,nombre_archivo_index);
                //printf("\tins inciol!\n");
                addRegistro(&r,sizeof(regindices),nombre_archivo_index);
                return ASSERT;
            }

            r = Ins_EntreNodos(r,nombre_archivo_index,lang);
           // printf("\tins enemdiol!\n");
            addRegistro(&r,sizeof(regindices),nombre_archivo_index);
            return ASSERT;
        }
    }else{
        addRegistro(&r,sizeof(regindices),nombre_archivo_index);
        setInicio(lang,r.Palabra[0]-97,getInicio(r.Palabra[0],lang),numRegistrosExistentes(sizeof(regindices),nombre_archivo_index)-1,nombre_archivo_inis);
    }
}

#endif // MANREGIDX_H_INCLUDED
