#ifndef CONTROLISERTTRAD_H_INCLUDED
#define CONTROLISERTTRAD_H_INCLUDED
#include <string.h>
#include "../structs/Estructuras.h"
#include "../structs/config.h"
#include "../file_man/modelo.h"

traduccion ultimaTrad(int pos_lista,int idioma, int pos_ri_esp, int pos_ri_ing, int * aux2){
    traduccion * aux = (traduccion *) getRegistro(sizeof(traduccion),pos_lista,nombre_archivo_trn);
    //traduccion dummy = getTraduccionVacio();
    if(idioma == ESPANIOL){
        if((*aux).sigIng ==-1){
                 *aux2 = pos_lista;

        }
        while((*aux).sigIng != -1){
            aux = (traduccion *) getRegistro(sizeof(traduccion),(*aux).sigIng,nombre_archivo_trn);
            if((*aux).sigIng!= -1){
                *aux2 = (*aux).sigIng;
            }
        }
    }else{
        while((*aux).sigEsp != -1){
            aux = (traduccion *) getRegistro(sizeof(traduccion),(*aux).sigEsp,nombre_archivo_trn);
            if((*aux).sigEsp!= -1){
                *aux2 = (*aux).sigEsp;
            }
        }
    }
    return *aux;
}

traduccion buscaEnLista(int pos_lista,int idioma, int pos_ri_esp, int pos_ri_ing){
    traduccion * aux = (traduccion *) getRegistro(sizeof(traduccion),pos_lista,nombre_archivo_trn);
    traduccion dummy = getTraduccionVacio();
    if(idioma == ESPANIOL){
        while((*aux).sigIng != -1){
            aux = (traduccion *) getRegistro(sizeof(traduccion),(*aux).sigIng,nombre_archivo_trn);
            if((*aux).posRegEsp == pos_ri_esp && (*aux).posRegIng == pos_ri_ing){
                return dummy;
            }
        }
    }else{
        while((*aux).sigEsp != -1){
            aux = (traduccion *) getRegistro(sizeof(traduccion),(*aux).sigEsp,nombre_archivo_trn);
            if((*aux).posRegEsp == pos_ri_esp && (*aux).posRegIng == pos_ri_ing){
                return dummy;
            }
        }
    }
    return *aux;

}

int controlInsertTrad(regindices * trad_esp,regindices * trad_ing,int idioma){
    int lang_list, tam, penultimo;
    traduccion n_trad = getTraduccionVacio(), aux;
    regindices generico;


    if(idioma == ESP_A_ING){
        lang_list = ESPANIOL;
    }else{
        lang_list = INGLES;
    }

    if(idioma = ESP_A_ING){
        if((*trad_esp).numReg_trad==-1){
            n_trad.posRegEsp = (*trad_esp).numReg_datos;
            n_trad.posRegIng = (*trad_ing).numReg_datos;

        }else{
            n_trad = buscaEnLista((*trad_esp).numReg_trad,lang_list,(*trad_esp).numReg_datos, (*trad_ing).numReg_datos);
        }
        generico = *trad_esp;
    }else{
        if((*trad_ing).numReg_trad==-1){
            n_trad.posRegEsp = (*trad_esp).numReg_datos;
            n_trad.posRegIng = (*trad_ing).numReg_datos;

        }else{
            n_trad = buscaEnLista((*trad_ing).numReg_trad,lang_list,(*trad_esp).numReg_datos, (*trad_ing).numReg_datos);
        }
        generico = *trad_ing;
    }
    if(n_trad.posRegEsp != -1 && n_trad.posRegIng != -1){
        tam = numRegistrosExistentes(sizeof(traduccion),nombre_archivo_trn);
        n_trad = getTraduccionVacio();
        n_trad.posRegEsp = (*trad_esp).numReg_datos;
        n_trad.posRegIng = (*trad_ing).numReg_datos;
        if(generico.numReg_trad==-1){
            addRegistro(&n_trad,sizeof(traduccion),nombre_archivo_trn);
            generico.numReg_trad = tam;
            setRegistro(&generico,generico.numReg_datos,nombre_archivo_index);
        }else{
            aux = ultimaTrad(generico.numReg_trad,lang_list,(*trad_esp).numReg_datos, (*trad_ing).numReg_datos, &penultimo);
            if(lang_list == ESPANIOL){
                aux.sigIng=tam;
            }else{
                aux.sigEsp=tam;
            }
            setRegistro(&aux,penultimo,nombre_archivo_trn);
            addRegistro(&n_trad,sizeof(traduccion),nombre_archivo_trn);
        }
        return ASSERT;
    }else{
        return FAIL;
    }

}

#endif // CONTROLISERTTRAD_H_INCLUDED
