
/**
 *	Header modelo.h que contiene las funciones para acceder a los archivos y
    hacer las modificaciones necesarias

 *
 **/

#ifndef MODELO_H_INCLUDED
#define MODELO_H_INCLUDED
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../structs/Estructuras.h"
#include "../structs/config.h"

void despliegaArchivo(){
    FILE *arch=fopen(nombre_archivo_inis,"rb");
    fseek(arch,0,SEEK_SET);
    inicio reg;
    while(!feof(arch)){
        fread(&reg,sizeof(inicio),1,arch);
        printf("\n%d-%d",reg.inicioIndexEsp,reg.inicioIndexIngles);
    }
}

/**
    Funcion que accede al archivo especificado
    para escribir un  registro en forma de adicion
    Parametros  reg void* registro  a escribir
                sz int tamaño del registro para distinguir entre estructuras
                char *nombreArchivo donde se escribira el registro
    return 1

*/

int addRegistro(void * reg, int sz, char * nombreArchivo){
	FILE *arch = fopen(nombreArchivo, "ab+");

	fwrite(reg,sz,1,arch);
	fclose(arch);
	//printf("\nRegistro agregado!!\n");

	return 1;
}

/**
    Funcion que accede al archivo para hacer una lectura de un
    registro de tamaño variable en cualquier posicion
    Parametros  tamReg int tamaño del registro en bytes
                pos int poscion del registro en el archivo
                nombreArchivo char* nombre delarchivo al que se accede
    return reg void*

*/

void * getRegistro(int tamReg,int pos,char *nombreArchivo) {
    void * reg;
    //inicio * r;
    //inicio r2;

    FILE * arch = fopen(nombreArchivo, "rb");
    if (arch == NULL) {
        // no se pudo abrrir el archivo
        return NULL;
    }

    reg = malloc(tamReg);
    fseek(arch, pos*tamReg, SEEK_SET);
    fread(reg, tamReg, 1, arch);
    //r = (inicio*)reg;
    //r2= *r;


    fclose(arch);
    return reg;

}

/**
    Funcion que calcula y retorna el tamaño de la estructura correspondiente al archivo que pasa como parametro
    Parametros  char *nombre_arch
    return int tamaño de la estructura

*/

int calculaTamanio(char *nombre_arch){
    if(strcmp(nombre_arch,nombre_archivo_dats)==0){
        return sizeof(registro);
    }else if(strcmp(nombre_arch,nombre_archivo_index)==0){
        return sizeof(regindices);
    }else if(strcmp(nombre_arch,nombre_archivo_inis)==0){
        return sizeof(inicio);
    }else if(strcmp(nombre_arch,nombre_archivo_trn)==0){
        return sizeof(traduccion);
    }
    return -1;
}

/**
    Funcion que accede al archivo especificado
    para hacer una sobreescritura de un registro en el archivo
    Parametros  reg void* registro  a escribir
                pos int posicion donde se sobreescribe
                nombreArchivo char* archivo al que se accede para escribir
    return 1

*/

int setRegistro(void * reg, int pos, char * nombreArchivo) {
    FILE * arch = fopen(nombreArchivo, "rb+");
        // no se pudo abrrir el archivo
    if(!arch){
        return 0;//NUll
    }
    //reg = (regHash *) malloc(sizeof (regHash));
    int tamanio = calculaTamanio(nombreArchivo);
    fseek(arch, pos*tamanio, SEEK_SET);
    fwrite(reg, tamanio, 1, arch);
    fclose(arch);
   // printf("\nRegistro modificado!!\n");
    return 1;
}

/**
    Funcion que calcula el numero de registro en un archivo
    Parametros  sz int tamaño del registro en el archivo
                char *nombreArchivo del que se calcula el numero de registros
    return int numero de registro en el archivo

*/

int numRegistrosExistentes(int sz,char *nombre_archivo){
    int numReg;
    FILE *arch;
    arch = fopen(nombre_archivo, "rb+");
    if(arch == NULL){
        return -99;
    }
    fseek(arch, 0L, SEEK_END);
    numReg = (int)ftell(arch);
    rewind(arch);
    fclose(arch);

    return numReg/sz;
}

/**Funcion que crea el archivo binario correspondiente vacio
    inicializa nuestro archivo donde guardamos los apuntadores a inicio de las listas
    parametros char * nombre_archivo
    return 1

*/

int creaArchivos(char * nombre_archivo){

    inicio regVacio = getInicioVacio();
    int i;
    FILE * arch = fopen(nombre_archivo, "rb");
    if(!arch){
        arch = fopen(nombre_archivo, "wb");
        if(strcmp(nombre_archivo,nombre_archivo_inis)==0){

            for(i=0;i<=26;i++){
                fwrite(&regVacio,sizeof(inicio),1,arch);
            }
        }

         if(!arch){
            printf("\nError, no se pudo crear el archivo %s.\n Puede que tengas problemas al utilizar el programa",nombre_archivo);
            return ERROR_FILE;
         }

    }
    fclose(arch);
    return 1;
}

/**Pseudocodigo

    funcion creaArchivos(nombre de archivo): void
    si ya esta creado el archivo
        no hace nada

    sino
        es su primera ejecucion
        Solamente se crea el archivo
        inicializa los archivos de datos,traducciones e indices
        si es el archivo de inicios
            se llena el archivo con estructuras vacias de tipo inicio

*/


/**Funcion principal que invoca a crear los archivos correspondientes
    parametros
    return 1

*/
int firstRun(){
    creaArchivos(nombre_archivo_dats);
    creaArchivos(nombre_archivo_index);
    creaArchivos(nombre_archivo_trn);
    creaArchivos(nombre_archivo_inis);
    return 1;
}

/**
    Funcion que busca en el archivo de indices la palabra para validar que no haya registros repetidos
    return 1

*/

int getInicio(char letra, int idioma ){
    int inicioLista;
    inicio * in;
    //inicio rastreador;
    int nReg;
    //regindices reg;
    //char = palabra [0]:
    nReg =  letra - 97; //(ASCII de A
    in = (inicio*) getRegistro (sizeof (inicio), nReg, nombre_archivo_inis);
    if(idioma==ESPANIOL){
        inicioLista = in-> inicioIndexEsp;
    }
    else {
        inicioLista = in->inicioIndexIngles;
    }
    //rastreador = *in;
    return inicioLista;

}

regindices buscaPalabra(char* palabra,int idioma){
    regindices reg;
    inicio *r;
    inicio regi;

    int inicioLista = getInicio(palabra[0],idioma);
 /*   r=(inicio*)getRegistro(sizeof(inicio),inicioLista,nombre_archivo_inis);
    int inicioIdioma;
    regi = *r;
    if(idioma==ESPANIOL){
        inicioIdioma = r->inicioIndexEsp;
    }else{
        inicioIdioma = r->inicioIndexIngles;
    }
*/
    if(inicioLista!=-1){

        FILE * arch = fopen(nombre_archivo_index,"rb");
        fseek (arch,(sizeof(regindices))* inicioLista,SEEK_SET);
        fread (&reg, sizeof(regindices), 1, arch);
        if(reg.sig==-1){
            return reg;
        }
        while(reg.sig != -1){

            if(strcmp(reg.Palabra,palabra)==0){///PODRIA CAMBIAR PARA QUE NO RRECORRA TODA LA LISTA EN CASO DE QUE NO EXISTA
                return reg;
            }
            fseek(arch,sizeof(reg)*reg.sig,SEEK_SET);
            fread(&reg, sizeof(regindices), 1, arch);
            if(reg.sig == -1){
                if(strcmp(reg.Palabra,palabra)==0){///PODRIA CAMBIAR PARA QUE NO RRECORRA TODA LA LISTA EN CASO DE QUE NO EXISTA
                    return reg;
                }
            }
        }
    }else{
        return getIndicesVacio();
    }

    return getIndicesVacio();
}

#endif // MODELO_H_INCLUDED
