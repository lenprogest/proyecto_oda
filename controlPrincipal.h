
/**
 *	Header controlPrincipal.h que contiene los controladores principales para el funcionamiento del programa
 **/

#ifndef CONTROLPRINCIPAL_H_INCLUDED
#define CONTROLPRINCIPAL_H_INCLUDED

#include "structs/Estructuras.h"
#include "structs/config.h"
#include "views/menu_Vistas.h"
#include "modelo.h"
#include "file_man/manRegIdx.h"
#include "views/vistaDatos.h"
#include "file_man/manRegIdx_del.h"
#include "file_man/controlIsertTrad.h"
#include "views/vistaTraducciones.h"
     /**********************************

              Menu Español

      **********************************/
/**
 *	Funcion que lleva el control logico de las opciones del menu en espaniol
 **/
void menuEspaniol(){
    int opcion;
    //insertar
    registro datos =getRegistroVacio();
    registro dts;
    regindices regidx = getIndicesVacio();

    char palabra[MAX_PALABRA];
    regindices palIndice;
    registro *regDatos;
    registro regModDatos;


    //modidficar
    regindices regidxNuevo = getIndicesVacio();


	do{
		system("cls");
		opcion = opcionEspaniol();
     	fflush(stdin);
		//scanf("%d", &opcion);


     	switch(opcion){
     		case 1:
     			system("cls");
     			//datos = pideDatosPalabra();
     			dts = pideDatosPalabra();
     			strcpy(regidx.Palabra,dts.palabra);
     			strcpy(regidx.Palabra,dts.palabra);
//                regidx.Palabra = dts.palabra;
                regidx.numReg_datos = numRegistrosExistentes(sizeof(datos),nombre_archivo_dats);

     			if(buscaTipoIns(regidx,ESPANIOL)==ASSERT){
                    addRegistro((void*)&dts,sizeof(registro),nombre_archivo_dats);
                    printf("\n  REgistro agregado con exito");
     			}else{
                    printf("\n  No se pudo agregar el registro al archivo");
     			}
     			system("pause");
     		break;

     		case 2:
     		    //eliminar
     			system("cls");

     			mostrarPalabrasAll(ESPANIOL);
     			strcpy(palabra,pidePalabra(2).Palabra);
     			//devuleve int
     			if(eliminarReg(ESPANIOL,palabra)==ASSERT){
                    printf("\n Registro eliminado con exito");
     			}else{
                    printf("\n No se pudo eliminar el registro");
     			}
     			system("pause");

     		break;

     		case 3:
     			system("cls");
     			mostrarPalabrasAll(ESPANIOL);
     			//ModificaDef;
     			strcpy(palabra,pidePalabra(3).Palabra);
     			//palabra = pidePalabra(3);
     			palIndice = buscaPalabra(palabra,ESPANIOL);//buscamos si existe
                //obtenemos el registro de la tabla de datos
                regDatos = (registro*)getRegistro(sizeof(registro),palIndice.numReg_datos,nombre_archivo_dats);
                //mandamos a modificar el registro
                regModDatos = modificaDatos(*regDatos);
                //si hizo cambios en la palabra
                if(strcmp((*regDatos).palabra,regModDatos.palabra)!=0){
                    eliminarReg(ESPANIOL,(*regDatos).palabra);

                    strcpy(regidxNuevo.Palabra,regModDatos.palabra);
                    strcpy(regidxNuevo.Palabra,regModDatos.palabra);
//                  regidx.Palabra = dts.palabra;
                    regidx.numReg_datos = numRegistrosExistentes(sizeof(datos),nombre_archivo_dats);

                    buscaTipoIns(regidxNuevo,ESPANIOL);
                    addRegistro((void*)&regModDatos,sizeof(registro),nombre_archivo_dats);
                    printf("\n  Registro Modificado Exitosamente");

                }else{
                    setRegistro(&regModDatos,palIndice.numReg_datos,nombre_archivo_dats);
                    printf("\n  Registro Modificado Exitosamente");
                }
				system("pause");
     		break;

     		case 4:
     			system("cls");
     			//BuscarDefinicion();
     			strcpy(palIndice.Palabra,pidePalabra(4).Palabra);
     			palIndice = buscaPalabra(palIndice.Palabra,ESPANIOL);

     			if(strcmp(palIndice.Palabra,getIndicesVacio().Palabra)==0){
                    printf("\nPalabra no existente en el diccionario");
     			}else{
     			//palIndice.Palabra = pidePalabra(4);
                    regDatos = (registro*)getRegistro(sizeof(registro),palIndice.numReg_datos,nombre_archivo_dats);
                    encabezadoDatos();
                    despliegaRegistro(*regDatos);
                    system("pause");
     			}
     		break;

     		case 5:
     			system("cls");
     			if(numRegistrosExistentes(sizeof(regindices),nombre_archivo_index)!=0){
                    mostrarPalabras(ESPANIOL);
                }else{
                    printf("\n  Ningun registro en el archivo");
                }
     			//despliegaArchivo();
				system("pause");
     		break;

     		case 6:
     			system("cls");
     		break;

     		default:
     			printf("\n OPCION INCORRECTA\n");
     	}
     	//getchar();
	}while (opcion != 0);
}
     /**********************************

              Menu Ingles

      **********************************/

/**
 *	Funcion que lleva el control logico de las opciones del menu en ingles
 **/
void menuIngles(){
    int opcion;

    registro datos =getRegistroVacio();
    registro dts;
    regindices regidx = getIndicesVacio();

    char palabra[MAX_PALABRA];
    regindices palIndice;
    registro *regDatos;
    registro regModDatos;

    regindices regidxNuevo;
	do{
		system("cls");
		opcion = opcionIngles();
     	fflush(stdin);

     	switch(opcion){
     		case 1:
     			system("cls");
     			//AgregarPalabra();
     				//datos = pideDatosPalabra();
     			dts = pideDatosPalabra();
     			strcpy(regidx.Palabra,dts.palabra);
     			strcpy(regidx.Palabra,dts.palabra);
//                regidx.Palabra = dts.palabra;
                regidx.numReg_datos = numRegistrosExistentes(sizeof(datos),nombre_archivo_dats);

     			if(buscaTipoIns(regidx,INGLES)==ASSERT){
                    addRegistro((void*)&dts,sizeof(registro),nombre_archivo_dats);
                    printf("\n  REgistro agrgado con exito");
     			}else{
                    printf("\n  No se pudo agregar el registro al archivo");
     			}
     			system("pause");
     			system("pause");
     		break;

     		case 2:
     			system("cls");
     			mostrarPalabrasAll(INGLES);
     			strcpy(palabra,pidePalabra(2).Palabra);
     			//devuleve int
     			if(eliminarReg(INGLES,palabra)==ASSERT){
                    printf("\n Registro eliminado con exito");
     			}else{
                    printf("\n No se pudo eliminar el registro");
     			}
     			system("pause");

     		break;

     		case 3:
     			system("cls");
     			//ModificaDef;
     			//ModificaDef;
     			mostrarPalabrasAll(INGLES);
     			strcpy(palabra,pidePalabra(3).Palabra);
     			//palabra = pidePalabra(3);
     			palIndice = buscaPalabra(palabra,INGLES);//buscamos si existe
                //obtenemos el registro de la tabla de datos
                regDatos = (registro*)getRegistro(sizeof(registro),palIndice.numReg_datos,nombre_archivo_dats);
                //mandamos a modificar el registro
                regModDatos = modificaDatos(*regDatos);
                //si hizo cambios en la palabra
                if(strcmp((*regDatos).palabra,regModDatos.palabra)!=0){
                    eliminarReg(INGLES,(*regDatos).palabra);

                    strcpy(regidxNuevo.Palabra,regModDatos.palabra);
                    strcpy(regidxNuevo.Palabra,regModDatos.palabra);
//                  regidx.Palabra = dts.palabra;
                    regidx.numReg_datos = numRegistrosExistentes(sizeof(datos),nombre_archivo_dats);

                    buscaTipoIns(regidxNuevo,INGLES);
                    addRegistro((void*)&regModDatos,sizeof(registro),nombre_archivo_dats);

                }else{
                    setRegistro(&regModDatos,palIndice.numReg_datos,nombre_archivo_dats);
                }
				system("pause");
     		break;

     		case 4:
     			system("cls");
     			//BuscarDefinicion();
     			strcpy(palIndice.Palabra,pidePalabra(4).Palabra);
     			palIndice = buscaPalabra(palIndice.Palabra,INGLES);

     			if(strcmp(palIndice.Palabra,getIndicesVacio().Palabra)==0){
                    printf("\nPalabra no existente en el diccionario");
     			}else{
     			//palIndice.Palabra = pidePalabra(4);
                    regDatos = (registro*)getRegistro(sizeof(registro),palIndice.numReg_datos,nombre_archivo_dats);
                    encabezadoDatos();
                    despliegaRegistro(*regDatos);
                    system("pause");
     			}

				system("pause");
     		break;

     		case 5:
     			system("cls");
     			//Mostrar();
     			if(numRegistrosExistentes(sizeof(regindices),nombre_archivo_index)!=0){
                    mostrarPalabras(INGLES);
                }else{
                    printf("\n  Ningun registro en el archivo");
                }
				system("pause");
     		break;

     		case 0:
     			system("cls");
     		break;

     		default:
     			printf("\n OPCION INCORRECTA\n");
     	}
     	getchar();
	}while (opcion != 0);
}

      /**********************************

              Menu Traducciones

      **********************************/

/**
 *	Funcion que lleva el control logico de las opciones del menu traducciones
 **/
void menuTraducciones(){
    int opcion;
    regindices palabraAtraducir;
    regindices palabratraducida;
	do{
		system("cls");
		opcion = opcionTraducciones();
     	fflush(stdin);

     	switch(opcion){
     		case 1:
     			system("cls");
     			palabraAtraducir = vistapalabraATraducir(ESPANIOL);
     			if(strcmp(palabraAtraducir.Palabra,getIndicesVacio().Palabra)==0){
                    printf("\nPalabra no encontrada en el archivo");
     			}else{
                    palabratraducida = vistapalabraTraducion(INGLES);
                    if(strcmp(palabraAtraducir.Palabra,getIndicesVacio().Palabra)==0){
                        printf("\nPalabra no encontrada en el archivo");
                    }else{
                        controlInsertTrad(&palabraAtraducir,&palabratraducida,ESP_A_ING);
                    }
     			}//Ag}}regarPalabra();

     			system("pause");
     		break;

     		case 2:
     			system("cls");
     			//ModificarPalabra();
     			system("pause");

     		break;

     		case 3:
     			system("cls");
     			//EliminarPalabra();
				system("pause");
     		break;

     		case 4:
     			system("cls");
     			//ConsultarPalabra();
     			despliegaTraducciones();
				system("pause");
     		break;

     		case 5:
     			system("cls");
     			//AgregarPalabra();
				system("pause");
     		break;

            case 6:
     			system("cls");
     			//ModificarPalabra();
				system("pause");
     		break;

            case 7:
     			system("cls");
     			//EliminarPalabra();
				system("pause");
     		break;

            case 8:
     			system("cls");
     			//ConsultarPalabra();
				system("pause");
     		break;

     		case 9:
     			system("cls");
     		break;

     		//default:
     			//printf("\n OPCION INCORRECTA\n");
     	}
     	//getchar();
	}while (opcion != 9);

}

/**
 *	Funcion que lleva el control logico de las opciones del menu principal
 **/
int menuPrincipal()
{
	int res,bandera;
	do{
		system("cls");
     	fflush(stdin);
		res=opcionPrincipal();
		bandera=0;

		switch(res)
		{
		case 1:
            printf("Eligio menu Espaniol\n\n");
            system("pause");
			menuEspaniol();
			getchar();
			break;
		/*case 1:
		    printf("Eligio menu Espaniol\n\n");

		    system("pause");
			menuEspaniol();
			getchar();
			break;

    */
		case 2:
			printf("Eligio menu Ingles\n\n");
		    system("pause");
		    menuIngles();
			getchar();
			break;
		/*case 9:
			printf("Eligio menu Ingles\n\n");
		    system("pause");
		    menuIngles();
			getchar();
			break;

*/
		case 3:
			printf("Eligio menu Traducciones\n\n");
		    system("pause");
		    menuTraducciones();
			getchar();
			break;
		/*case 20:
			printf("Eligio menu Traducciones\n\n");
		    system("pause");
		    menuTraducciones();
			getchar();
			break;
*/

		case 4:
			printf("Eligio menu Administracion\n\n");
		    system("pause");
			getchar();
			break;
	/*	case 1:
			printf("Eligio menu Administracion\n\n");
		    system("pause");
			getchar();
			break;
*/

        case 5:
            printf("Saliendo\n\n");
			bandera=1;
			getchar();
			break;
			/*
        case 19:
			printf("Saliendo\n\n");
			bandera=1;
			getchar();
			break;
*/

		default:
			printf("opcion invalida\n");
		break;

		}

	} while(bandera!=1);

	return 0;
}

      /**********************************

              Menu Administracion

      **********************************/

      //No se a que se refiera esto




 ///Tal vez nos sirva lo de bajo o tal vez no----------------------------------------------------------------

      /**********************************

              Estructura

      **********************************/



/**
void mostrarPalabra(){
	int i=0;

	printf("\n              CONSULTA DE TODAS LAS PALABRAS\n\n");
	archm=fopen("Palabra.dat","rb");		//ABRE EL ARCHIVO

	if(archm == NULL){
		printf("\n\n EXISTE UN PROBLEMA CON EL ARCHIVO (NO SE HA CREADO O SE HA ELIMINADO)\n");
		printf("\n                INTENTE MAS TARDE\n\n");
		system("PAUSE");
		return;
	}


	fread(&palabr, sizeof(palabr), 1, archm);
	while(!feof(archm)){		//CICLO MIENTRAS HASTA QUE NO ENCUENTRE EL FINAL DEL ARCHIVO
		if(palabr.palabra != NULL){
			printf("   %5s               %s            %s\n\n", palabr.palabra, palabr.definicion,palabr.fuente);
			i++;
		}
		fread(&palabr, sizeof(palabr), 1, archm);
	}
	fclose(archm);
	printf("  -----------------------------------------------------\n");
	printf("             Total de palabras: %d\n\n", i);

	if(i==0){
		printf("\n\n    EL ARCHIVO ESTA CREADO PERO NO HAY REGISTROS EN EL");
	}
	return;
}
*/

#endif // CONTROLPRINCIPAL_H_INCLUDED
