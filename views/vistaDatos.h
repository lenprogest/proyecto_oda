
/**
 *	Header vistaDatos.h que contiene las vistas para desplegar al hacer una
    insercion,modificacion,eliminacion y consulta de algun registro en el archivo de datos
 *
 **/
#ifndef VISTADATOS_H_INCLUDED
#define VISTADATOS_H_INCLUDED

#include "../structs/Estructuras.h"
#include "../structs/config.h"
#include "../Prac5OrgArc/leeCadena.h"


/**
 *	Funcion que pide al ususario ingrese los datos de una estructura tipo registro
 * 	parametros:
 * 	@return reg registro
 *
 **/

registro pideDatosPalabra(){ //AGREGAR INFORMACION DE UN MEDIO
	registro reg;

	system("cls");

	printf("\Ingresa Palabra ");
    leeCadena(reg.palabra);
	//palabr.palabra = nom_pala;
	printf("\n Definicion: ");
	fflush(stdin);
	leeCadena(reg.descripcion);

   // palabr.palabra = nom_pala;
	printf("\n Fuente: ");
	fflush(stdin);
	leeCadena(reg.fuente);

	return reg;
}

/**
 *	Funcion que pide al usuario ingrese la palabra que desea eliminar
 * 	@parametros FUNCION
 * 	@return palabra char*
 *
 **/

regindices pidePalabra(int funcion){
    regindices reg;
    if(funcion==2){
        printf("\n      Dame la palabra que deseas eliminar: ");
        leeCadena(reg.Palabra);
    }else if(funcion==3){
        printf("\n      Dame la palabra que deseas modificar: ");
        leeCadena(reg.Palabra);
    }else if(funcion==4){
        printf("\n      Dame la palabra que deseas consultar su significado: ");
        leeCadena(reg.Palabra);
    }

    return reg;
 }

/**
 *	Funcion que despliega un registro de tipo reg (Contiene Datos: palabra,definicion,fuente)
 * 	parametros:
 * 	reg datos -> estructura que contiene los valores pedidos al usuario.
 *
 **/

void despliegaRegistro(registro datos){
    printf("%s  %s  %s \n",datos.palabra,datos.descripcion,datos.fuente);
}

/**
 *	Funcion que pide el campo del registro a modificar
 * 	int opc -> Donde se guardan el campo del resgistro a modificar.
 * 	return opc -> Regresa el campo elegido por el usuario.
 *
 **/
int pideCambiosDatos(){
    int opc;
    printf("\n  Campo del registro a modificar:\n    1.Palabra\n    2.Definicion\n    3.Fuente\n 6.Salir de este menu\n");
    scanf("%i",&opc);
    return opc;
}

/**
 *	Funcion que imprime en consola el encabezado de la informacion.
 **/
void encabezadoDatos(){
    printf("    PALABRA                     DEFINICION                    FUENTE\n");
	printf("  --------------------------------------------------------------------\n");
}

/**
 *	Funcion que hace cambios temporales en el registro especificado.
 *	parametros:
 *	reg datos ->registro a modificar
 *
 * 	return datosModif OR datos -> nuevos valores asignados al registro
**/
registro modificaDatos(registro datos){
        int opc = 0;
        int guarda_cambios = 0;
        int cambios_hechos = 0;
        registro datosModif = datos;
        printf("Registro a modificar:\n");
        encabezadoDatos();
        despliegaRegistro(datos);
	while(opc<=4){
            opc = pideCambiosDatos();
            switch(opc){
            case 1:
                printf("Nueva palabra del registro: ");
                leeCadena(datosModif.palabra);
                cambios_hechos = 1;
                break;
            case 2:
                printf("Nueva definicion de la palabra : ");
                leeCadena(datosModif.descripcion);
                cambios_hechos = 1;
                break;
            case 3:
                printf("Nueva Fuente de la definicion: ");
                leeCadena(datosModif.fuente);
                cambios_hechos = 1;
                break;
            default:
                if(cambios_hechos==1){
                    printf("Registro original\n");
                    despliegaRegistro(datos);
                    printf("\n");
                    printf("Registro modificado\n");
                    despliegaRegistro(datosModif);
                    printf("\n\nAntes de salir del menu Modificar...\n");
                    printf("Desea guardar los cambios?\n              1.SI  2.NO :          ");
                    scanf("%d",&guarda_cambios);
                }else{
                    printf("No se ha realizado ningun cambio\n");
                }
            }
		}
		if(guarda_cambios == 1){
            return datosModif;
		}else{
            return datos;
		}
}

void despliegaindices(regindices r){
    printf("\n%s",r.Palabra);
}
/**
 *	Funcion que recorre una lista de palabras de una letra inicial comun
 *	parametros:
 *	int inicioLista ->para saber donde empezarr a rrecorrer la lista
 *
 * 	return void
**/

void despliegaPalporLetra(int inicioLista,int idioma){
    regindices reg;
    registro *regdatos;
    registro regPrueba;
    inicio* in;
    int inicioListaDesplegar;
    in=(inicio*)malloc(sizeof(inicio));
        FILE * arch;

        in=getRegistro(sizeof(inicio),inicioLista,nombre_archivo_inis);
    if(idioma==ESPANIOL){
        inicioListaDesplegar=in->inicioIndexEsp;
    }else{
        inicioListaDesplegar=in->inicioIndexIngles;
    }

    if(inicioListaDesplegar!=-1){
        //lee de incices
        arch = fopen(nombre_archivo_index,"rb");
        fseek (arch,(sizeof(regindices))* inicioListaDesplegar,SEEK_SET);
        fread (&reg, sizeof(regindices), 1, arch);

        regdatos = (registro*)getRegistro(sizeof(registro),reg.numReg_datos,nombre_archivo_dats);
        regPrueba = *regdatos;
            if(reg.sig == -1){
                    //despliegaRegistro(*regdatos);
                    despliegaRegistro(*regdatos);
            }
            while(reg.sig != -1){
                despliegaRegistro(*regdatos);
                //despliegaRegistro(*regdatos);
                fseek(arch,sizeof(reg)*reg.sig,SEEK_SET);
                fread(&reg, sizeof(regindices), 1, arch);
                regdatos = (registro*)getRegistro(sizeof(registro),reg.numReg_datos,nombre_archivo_dats);
                if(reg.sig == -1){
                    //despliegaRegistro(*regdatos);
                    despliegaRegistro(*regdatos);
                }
            }
        }
}

/**
 *	Funcion que muestra la totalidad de las palabras segun desee el usuario
 *	parametros:
 *	int idioma ->para elegir diccionario
 *
 * 	return void
**/

void mostrarPalabras(int idioma){
    int opc;
    char caracter[2];
    int i;
    int pos;
    registro datos;
    int inicioLista;
//    char caracter;
    printf("\n1.Mostrar todas las palabras del diccionario\n2.Mostrar solo las palbras que inician con alguna letra en comun\n");
    scanf("%d",&opc);
    if(opc==2){

        printf("\n  Elija una opcion...");
        while(getchar()!='\n');
        printf("\n  Dame la letra inicial del conjunto de palabras que deseas consultar: ");
        scanf("%s",&caracter[0]);
        encabezadoDatos();
        pos=caracter[0]-97;
        //inicioLista = getInicio(pos,idioma);
        despliegaPalporLetra(pos,idioma);
    }else{
        encabezadoDatos();
        for(i=0;i<=26;i++){
            despliegaPalporLetra(i,idioma);
        }
    }
}




/**
 *	Funcion que recorre una lista de palabras de una letra inicial comun
 *	parametros:
 *	int inicioLista ->para saber donde empezarr a rrecorrer la lista
 *
 * 	return void
**/

void despliegaPalporLetraAll(int inicioLista,int idioma){
    regindices reg;
    registro *regdatos;
    registro regPrueba;
    inicio* in;
    int inicioListaDesplegar;
    in=(inicio*)malloc(sizeof(inicio));
        FILE * arch;

        in=getRegistro(sizeof(inicio),inicioLista,nombre_archivo_inis);
    if(idioma==ESPANIOL){
        inicioListaDesplegar=in->inicioIndexEsp;
    }else{
        inicioListaDesplegar=in->inicioIndexIngles;
    }

    if(inicioListaDesplegar!=-1){
        //lee de incices
        arch = fopen(nombre_archivo_index,"rb");
        fseek (arch,(sizeof(regindices))* inicioListaDesplegar,SEEK_SET);
        fread (&reg, sizeof(regindices), 1, arch);

        regdatos = (registro*)getRegistro(sizeof(registro),reg.numReg_datos,nombre_archivo_dats);
        regPrueba = *regdatos;
            if(reg.sig == -1){
                    //despliegaRegistro(*regdatos);
                    despliegaindices(reg);
            }
            while(reg.sig != -1){
                despliegaindices(reg);
                //despliegaRegistro(*regdatos);
                fseek(arch,sizeof(reg)*reg.sig,SEEK_SET);
                fread(&reg, sizeof(regindices), 1, arch);
                regdatos = (registro*)getRegistro(sizeof(registro),reg.numReg_datos,nombre_archivo_dats);
                if(reg.sig == -1){
                    //despliegaRegistro(*regdatos);
                    despliegaindices(reg);
                }
            }
        }
}

/**
 *	Funcion que muestra la totalidad de las palabras segun desee el usuario
 *	parametros:
 *	int idioma ->para elegir diccionario
 *
 * 	return void
**/

void mostrarPalabrasAll(int idioma){
    int opc;
    int i;
    int pos;
    char caracter[2];
    registro datos;
    int inicioLista;
//    char caracter;
    printf("\n  Elija una opcion para desplegar los registros disponibles:");
    printf("\n1.Mostrar todas las palabras del diccionario\n2.Mostrar solo las palbras que inician con alguna letra en comun\n");
    scanf("%d",&opc);
    if(opc==2){
            while(getchar()!='\n');
        printf("\n  Dame la letra inicial del conjunto de palabras que deseas consultar: ");
        scanf("%s",&caracter[0]);
        ///encabezadoDatos();
        if(idioma==ESPANIOL){
            printf("\n\tPALABRAS EN ESPANOL\n");
            printf("-----------------------------------\n");
        }else{
            printf("\tPALABRAS EN INGLES\n");
            printf("-----------------------------------\n");

        }
         pos=caracter[0]-97;
        //inicioLista = getInicio(caracter,idioma);
        despliegaPalporLetra(pos,idioma);
    }else{
        ///encabezadoDatos();
         if(idioma==ESPANIOL){
            printf("\n\tPALABRAS EN ESPANOL\n");
            printf("-----------------------------------\n");
        }else{
            printf("\tPALABRAS EN INGLES\n");
            printf("-----------------------------------\n");

        }
        for(i=0;i<=26;i++){
            despliegaPalporLetraAll(i,idioma);
        }
    }
}
/**
 *	modificaDatos(datos reg): reg
        hacemos una copia de la estructura datos
        desplegamos los datos de la estructura que se quiere modificar
        Entramos a un ciclo donde puede modificarlos acmpos de la copia de la estructura
            pedimos que el usuario elija el campo a modificar
                pide el nuevo valor para el campo elegido
        si hizo algun cambio
            le despliega los campos del registro tanto el original como el modificado
            pregunta al usuario si desea guardar los cambios
        sino
            solo desplegamos un mensaje de que no se modifco nada

        si eligio guardar los cambios
            regresa la estructura modificada
        sino
            regresa la misma estructura del parametro de entrada
**/

#endif // VISTADATOS_H_INCLUDED
