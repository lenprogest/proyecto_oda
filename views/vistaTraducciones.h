#ifndef VISTATRADUCCIONES_H_INCLUDED
#define VISTATRADUCCIONES_H_INCLUDED
#include "../structs/Estructuras.h"
#include "../structs/config.h"
#include "../file_man/modelo.h"

/**
 *	Header vistaTraducciones.h que contiene las vistas para desplegar al hacer
    una insercion,modificacion,eliminacion y consulta de alguna traduccion
 *
 **/

regindices vistapalabraATraducir(int idioma){
     char palabra[MAX_PALABRA];
     printf("\nPalabra a traducir: ");
     scanf("%s",&palabra);
     regindices regTrad;
     regTrad = buscaPalabra(palabra,idioma);

    return regTrad;

 }


 regindices vistapalabraTraducion(int idioma){
     char palabra[MAX_PALABRA];
     printf("\nTraduccion de la palabra: ");
     scanf("%s",&palabra);
     regindices regTrad;
     regTrad = buscaPalabra(palabra,idioma);
     return regTrad;

 }


void despliegaTraducciones(){
    char palabra[MAX_PALABRA];
    traduccion *aux;
    aux=(traduccion*)malloc(sizeof(traduccion));
    traduccion aux2;

    regindices *reg;
    reg= (regindices*)malloc(sizeof(regindices));
    regindices prueba = getIndicesVacio();
    printf("\nDame la palabra de la que quieres saber sus traducciones: ");
    scanf("%s",&palabra);
    prueba=buscaPalabra(palabra,ESPANIOL);
    aux = (traduccion*)getRegistro(sizeof(traduccion),prueba.numReg_trad,nombre_archivo_trn);
    aux2 = *aux;
    if((*aux).sigIng == -1){
            reg = (regindices*)getRegistro(sizeof(regindices),(*aux).posRegIng,nombre_archivo_index);
            printf("    %s",(*reg).Palabra);

        }
    while((*aux).sigIng != -1){
        reg = (regindices*)getRegistro(sizeof(regindices),(*aux).posRegIng,nombre_archivo_index);
        aux = (traduccion *) getRegistro(sizeof(traduccion),(*aux).sigIng,nombre_archivo_trn);
        printf(",%s",(*reg).Palabra);
        if((*aux).sigIng == -1){
            reg = (regindices*)getRegistro(sizeof(regindices),(*aux).posRegIng,nombre_archivo_index);
            printf(",%s",(*reg).Palabra);

        }
    }
    printf("\n\n");
}



#endif // VISTATRADUCCIONES_H_INCLUDED
