#ifndef MENU_VISTAS_H_INCLUDED
#define MENU_VISTAS_H_INCLUDED
#include <stdio.h>

/**
 *	Header menu_Vistas.h
    contiene las vistas qie desplegara en menus para el usuario
 *
 **/


 /**
 *	Funcion opcionPrincipal
    desplega las opciones del menu principal
    return opc opcion seleccionada
 *
 **/
int opcionPrincipal(){
    int opcion;
		printf("\tMenu Principal\n");
		printf("1.- Espanol\n");
		printf("2.- Ingles\n");
		printf("3.- Traducciones\n");
		printf("4.- Administracion\n");
		printf("5.- Salir\n\n");
		printf("ELIGE UNA OPCION: \n\n");
    scanf("%i",&opcion);
    return opcion;
}

 /**	Funcion opcionEspa�ol
        despliega las opciones del menu espa�ol
        return opcion
 *
 **/

int opcionEspaniol(){
    int opcion;
    	printf("\tMENU ESPANIOL\n");
     	printf("1.- Agregar una palabra y definicion\n");
     	printf("2.- Eliminar una palabra y definicion\n");
     	printf("3.- Modificar la definicion de una palabra\n");
     	printf("4.- Buscar la definicion de una palabra\n");
     	printf("5.- Mostrar todas palabras y su definicion\n");
     	printf("0.- Regresar al menu anterior\n\n");
    scanf("%i",&opcion);
    return opcion;
}

/**	Funcion opcionIngles
        despliega las opciones del menu ingles
        return opcion
 *
 **/

int opcionIngles(){
    int opcion;
    	printf("\tMENU INGLES\n");
     	printf("1.- Agregar una palabra y definicion\n");
     	printf("2.- Eliminar una palabra y definicion\n");
     	printf("3.- Modificar la definicion de una palabra\n");
     	printf("4.- Buscar la definicion de una palabra\n");
     	printf("5.- Mostrar todas las palabras y su definicion\n");
     	printf("0.- Regresar al menu anterior\n\n");
    scanf("%i",&opcion);
    return opcion;
}

/**	Funcion opcionAdminnistracion
        despliega las opciones del menu administracion
        return opcion
 *
 **/

int opcionAdmin(){
    int opcion;
    printf("MENU ADMINISTRACION");
    //imprimir reporte en texto
    //???

    scanf("%i",&opcion);
    return opcion;
}

/**	Funcion opcionTraducciones
        despliega las opciones del menu Traducciones
        return opcion
 *
 **/

int opcionTraducciones(){
     int opcion;
     printf("\tMENU TRADUCCIONES\n");
     	printf("1.- Agregar una traducci�n a una palabra en Espa�ol\n");
     	printf("2.- Modificar una traducci�n a una palabra en Espa�ol\n");
     	printf("3.- Eliminar  una traducci�n a una palabra en Espa�ol\n");
     	printf("4.- Consultar traducciones de una palabra en Espa�ol\n");

     	printf("5.-	Agregar una traducci�n a una palabra en Ingl�s\n");
     	printf("6.- Modificar una traducci�n a una palabra en Ingl�s\n");
     	printf("7.- Eliminar  un traducci�n a una palabra en Ingl�s\n");
     	printf("8.- Consultar traducciones de una palabra en Ingl�s\n");
     	printf("9.- Regresar al menu anterior\n\n");
    scanf("%i",&opcion);
    return opcion;
}

#endif // MENU_VISTAS_H_INCLUDED
